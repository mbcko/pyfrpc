set -e

PY="/opt/python/cp39-cp39/bin/python"

rm -f dist/*


# Run tox (tests + linux wheels + amalgamation).

$PY -m pip install tox cython
$PY -m tox


# Create manylinux wheels.

for WHL in dist/*whl; do
    auditwheel repair $WHL -w wheelhouse/
done

rm -f dist/*whl
mv wheelhouse/* dist/
rm -rf wheelhouse
