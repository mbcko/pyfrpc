import os
import re


def get_header(label):
    return '#' * 80 + '\n# ' + label + '\n' + '#' * 80 + '\n\n'


def get_module(filename):
    with open(os.path.join('src/pyfrpc', filename), 'r') as f:
        sc = f.read()
        sc = sc.split('\n')

        for i,line in enumerate(sc):
            m = re.search(r'^( *)((from|import) +\..*)$', line)

            if not m:
                continue

            sc[i] = m.group(1) + 'pass  # ' + m.group(2)

    sc = '\n'.join(sc)
    sc = get_header('include "' + filename + '"') + sc + '\n'

    return sc


def main():
    os.environ['PYFRPC_NOEXT'] = '1'

    from setup import setup_args as s

    sf = ''
    sf += '#!/usr/bin/env python\n'
    sf += '# -*- coding: utf-8 -*-\n'
    sf += "WITH_EXT = False"

    sf += '\n"""\n'
    sf += 'Author:     {author}\n'.format(**s)
    sf += 'Version:    {version}\n'.format(**s)
    sf += 'Url:        {url}\n'.format(**s)
    sf += 'License:    {license}\n'.format(**s)
    sf += '\n'
    sf += open('LICENSE.txt').read()
    sf += '\n"""\n\n'

    sf += get_module('netcat.py')
    sf += get_module('client.py')
    sf += get_module('models.py')
    sf += get_module('compat.py')
    sf += get_module('_coding_base_py.py')
    sf += get_module('coding.py')

    sf += get_header('hidden imports')
    sf += '_FrpcEncoder = FrpcEncoder\n'
    sf += '_FrpcDecoder = FrpcDecoder\n'
    sf += 'WITH_EXT = False\n'
    sf += '\n'

    sf += get_header('main')
    sf += 'if __name__ == "__main__":\n'
    sf += '    main()\n'

    # Write to 'dist/pyfrpc.py'.
    PWD = os.path.dirname(os.path.realpath(__file__))
    os.makedirs(os.path.join(PWD, 'dist'), exist_ok=True)

    with open(os.path.join(PWD, 'dist', 'pyfrpc.py'), 'w') as f:
        f.write(sf)


if __name__ == '__main__':
    main()
