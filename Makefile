
MANYLINUX_X86="quay.io/pypa/manylinux1_x86_64"

.PHONY: build
build:
	docker pull $(MANYLINUX_X86)
	docker run -it -v $(PWD):$(PWD) -w $(PWD) $(MANYLINUX_X86) bash ./ci/build.sh
